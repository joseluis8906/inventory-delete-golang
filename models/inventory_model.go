package models

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//InventoryModel is a struct that represents a client of mongo
type InventoryModel struct {
	collection *mongo.Collection
}

//NewInventoryModel is a method for setup mongo
func NewInventoryModel() *InventoryModel {
	clientOptions := options.Client().ApplyURI("mongodb://jarvis-mongo:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
		return nil
	}

	return &InventoryModel{collection: client.Database("jarvis").Collection("inventories")}
}

//DeleteOne is a method for delete one product
func (model *InventoryModel) DeleteOne(product string) bool {
	res, err := model.collection.DeleteOne(context.TODO(), bson.D{{"product", product}})
	if err != nil {
		log.Fatal(err)
		return false
	}
	if res.DeletedCount == 0 {
		return false
	}
	return true
}
