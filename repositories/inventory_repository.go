package repositories

import "app/models"

//InventoryRepository is a structu that represent a inventory model
type InventoryRepository struct {
	model *models.InventoryModel
}

//NewInventoryRepository is a constructor for InventoryRepository
func NewInventoryRepository(model *models.InventoryModel) *InventoryRepository {
	return &InventoryRepository{
		model: model,
	}
}

//DeleteOne delete one row from database
func (repository *InventoryRepository) DeleteOne(product string) bool {
	return repository.model.DeleteOne(product)
}
