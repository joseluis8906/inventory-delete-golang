package controllers

import (
	"app/repositories"

	"github.com/gin-gonic/gin"
)

//InventoryController is a controller for inventory
type InventoryController struct {
	repository *repositories.InventoryRepository
}

//NewInventoryController is a constructor for inventory controller
func NewInventoryController(repository *repositories.InventoryRepository) *InventoryController {
	return &InventoryController{
		repository: repository,
	}
}

// DeleteInventory is a method to erase a inventory row from data base
func (controller *InventoryController) DeleteInventory(ctx *gin.Context) {
	res := controller.repository.DeleteOne(ctx.Param("product"))
	if res {
		ctx.String(200, "")
	} else {
		ctx.JSON(404, gin.H{
			"message": "Product not found",
		})
	}

}
